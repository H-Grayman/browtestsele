![Grabbing Data](/media/automation.jpg)

# Project: Automated Testing
**Tools in development**

## Installation
Clone and Go: `git clone https://H-Grayman@bitbucket.org/H-Grayman/browtestsele.git`

## Usage
Load and run with ```python test_name.py```

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D
## History
Giving it a go!
## Credits
N/A
## License
