import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

driver = webdriver.Chrome()
driver.get("http://159.89.55.249/index.php/contact/")
N = 20
actions = ActionChains(driver)
actions.send_keys(Keys.TAB * N)
actions.perform()

print("Closing the test.")
time.sleep(2)
driver.close()
