from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


driver = webdriver.Chrome()
driver.get("http://159.89.55.249/index.php/home/")
if not "Home" in driver.title:
    raise Exception("Site title is incorrect")

print("Closing the test.")
time.sleep(2)
driver.close()
