import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


driver = webdriver.Chrome()
driver.get("http://159.89.55.249/index.php/contact/")
time.sleep(5)

name = driver.find_element_by_name("your-name")
name.send_keys("Test Names")

email = driver.find_element_by_name("your-email")
email.send_keys("wang-doodles@slugworthmail.com")

subject = driver.find_element_by_name("your-subject")
subject.send_keys("Test Subject")

message = driver.find_element_by_name("your-message")
message.send_keys("This is the test message")


print("Closing the test.")
time.sleep(3)
driver.close()
